//
//  DetailViewController.swift
//  20180219-GokulNanda-NYCSchools
//
//  Created by GokulNanda on 20/02/18.
//  Copyright © 2018 GokulNanda. All rights reserved.
//

import UIKit
import MBProgressHUD

class DetailViewController: UIViewController {

    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var schoolLocationLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var viewModel: DetailViewModel!

    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem {
            schoolNameLabel?.text = detail.schoolName
            schoolLocationLabel?.text = detail.location
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        viewModel = DetailViewModel()
        tableView?.dataSource = self
        tableView?.rowHeight = UITableViewAutomaticDimension

        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var detailItem: Schools? {
        didSet {
            // Update the view.
            configureView()
        }
    }

    @IBAction func displaySATResults(_ sender: Any) {
        guard let dbn = detailItem?.dbn else {
            return
        }
        MBProgressHUD.showAdded(to: self.view, animated: true)
        viewModel.getSATResults(dbn: dbn, completion: {
            MBProgressHUD.hide(for: self.view, animated: true)
            if self.viewModel.satResults.isEmpty {
                UIAlertController.displaySimpleAlert(message: "Unable to fetch SAT results")
            } else {
                self.tableView.reloadData()
            }
        })
    }}

extension DetailViewController: UITableViewDataSource {
    // MARK: - Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSectionsToDisplay()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsToDisplay(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let score = viewModel.satResults[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        cell.textLabel!.text = score.schoolName
        cell.detailTextLabel!.text = "Test Takers: \(score.numOfSatTestTakers ?? "")\n" +
            "Critical Reading Avg Score: \(score.satCriticalReadingAvgScore ?? "")\n"
        return cell
    }
}
