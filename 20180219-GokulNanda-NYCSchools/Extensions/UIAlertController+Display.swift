//
//  UIAlertController+Display.swift
//  20180219-GokulNanda-NYCSchools
//
//  Created by GokulNanda on 20/02/18.
//  Copyright © 2018 GokulNanda. All rights reserved.
//

import Foundation
import UIKit

extension UIAlertController {
    class func displaySimpleAlert(message: String) {
        let rootVC = UIApplication.shared.windows.first?.rootViewController
        let alertController = UIAlertController(title: "Message",
                                                message: message,
                                                preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        rootVC?.present(alertController, animated: true, completion: nil)
    }
}
