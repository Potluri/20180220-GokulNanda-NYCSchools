//
//  SchoolsAPI.swift
//  20180219-GokulNanda-NYCSchools
//
//  Created by GokulNanda on 20/02/18.
//  Copyright © 2018 GokulNanda. All rights reserved.
//

import Foundation
import Moya

enum SchoolsAPI {
    case schools(limit: Int, offset: Int)
    case satResults(dbn: String)
}

extension SchoolsAPI: TargetType {
    var baseURL: URL {
        return URL(string: rootURL)!
    }
    
    var path: String {
        switch self {
        case .schools(_, _):
            return "/resource/97mf-9njv.json"
        case .satResults(_):
            return "/resource/734v-jeq5.json"
        }
    }
    
    var headers: [String: String]? {
        return ["X-App-Token": appToken]
    }
    
    var method: Moya.Method {
        switch self {
        case .schools(_, _),
             .satResults(_):
            return .get
        }
    }
    
    var parameters: [String: Any]? {
        var param:[String: Any]!
        switch self {
        case .schools(let limit, let offset):
            param = [
                "$limit": limit,
                "$offset": offset
            ]
        case .satResults(let dbn):
            param = [
                "dbn": dbn,
            ]
        }
        return param
    }
    
    var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }
    
    var sampleData: Data {
        switch self {
        case .schools(_, _):
            return stubbedResponse("schools")
        case .satResults(_):
            return stubbedResponse("sat")
        default:
            return Data()
        }
    }
    
    var task: Task {
        switch self {
        default:
            return .requestParameters(parameters: self.parameters!, encoding: self.parameterEncoding)
        }
    }
}

func stubbedResponse(_ filename: String) -> Data! {
    let bundle = Bundle.main
    let path = bundle.path(forResource: filename, ofType: "json")
    return (try? Data(contentsOf: URL(fileURLWithPath: path!)))
}
