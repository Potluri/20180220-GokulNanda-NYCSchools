//
//  NetworkAdapter.swift
//  20180219-GokulNanda-NYCSchools
//
//  Created by GokulNanda on 20/02/18.
//  Copyright © 2018 GokulNanda. All rights reserved.
//

import Foundation
import Moya
import Gloss
import MBProgressHUD

let standardErrorMessage = "Error occurred, Please try again later."

let appToken = "Cw1mMSxBciHxXME3Zpd0N3pv8"

let rootURL = "https://data.cityofnewyork.us"

#if STUB
let schoolsAPIProvider = MoyaProvider<SchoolsAPI>(stubClosure: MoyaProvider.immediatelyStub)
#else
let schoolsAPIProvider = MoyaProvider<SchoolsAPI>()
#endif

struct NetworkAdapter {

    static func request<T: Gloss.JSONDecodable>(_ target: SchoolsAPI, type: T.Type, success completion: @escaping ([T]) -> Void) {
        schoolsAPIProvider.request(target) { (result) in
            switch result {
            case let .success(moyaResponse):
                do {
                    let result = try moyaResponse.mapArray(type)
                    completion(result)
                } catch {
                    UIAlertController.displaySimpleAlert(message: "Some error occured, Please try again later.")
                }
            case let .failure(error):
                print(error)
                UIAlertController.displaySimpleAlert(message: error.errorDescription ?? standardErrorMessage)
            }
        }
    }
}
