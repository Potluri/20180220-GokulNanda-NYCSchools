//
//  MasterViewController.swift
//  20180219-GokulNanda-NYCSchools
//
//  Created by GokulNanda on 20/02/18.
//  Copyright © 2018 GokulNanda. All rights reserved.
//

import UIKit
import MBProgressHUD

class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    var viewModel: MasterViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.refreshControl?.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControlEvents.valueChanged)

        viewModel = MasterViewModel()
        tableView?.rowHeight = UITableViewAutomaticDimension
        
        if let split = splitViewController {
            let controllers = split.viewControllers
            // swiftlint:disable force_cast
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
        
        self.loadData()
    }
    
    @objc
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        viewModel.resetData()
        self.tableView.reloadData()
        loadData()
        refreshControl.endRefreshing()
    }
    
    func loadData() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        viewModel.loadMore(completion: {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.tableView.reloadData()
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let object = viewModel.schoolsList[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSectionsToDisplay()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsToDisplay(in: section)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let school = viewModel.schoolsList[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        cell.textLabel!.text = school.schoolName
        
        // Check if the last row number is the same as the last current data element
        if indexPath.row == viewModel.schoolsList.count - 1 {
            self.loadData()
        }

        return cell
    }
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        // UITableView only moves in one direction, y axis
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 10.0 {
            self.loadData()
        }
    }
}
