//
//  MasterViewModel.swift
//  20180219-GokulNanda-NYCSchools
//
//  Created by GokulNanda on 20/02/18.
//  Copyright © 2018 GokulNanda. All rights reserved.
//

import Foundation
import Gloss

class MasterViewModel {
    var schoolsList = [Schools]()
    
    // Where to start fetching items (database OFFSET)
    var offset = 0

    // a flag for when all database items have already been loaded
    var reachedEndOfItems = false
    
    let itemsPerBatch = 20

    func numberOfSectionsToDisplay() -> Int {
        return 1
    }
    
    func numberOfRowsToDisplay(in section: Int) -> Int {
        return schoolsList.count
    }
    
    func resetData() {
        self.offset = 0
        self.reachedEndOfItems = false
        schoolsList = [Schools]()
    }
    
    func loadMore(completion: @escaping () -> Void) {
        
        guard !self.reachedEndOfItems else {
            return
        }
        
        DispatchQueue.global(qos: .background).async {
            // determine the range of data items to fetch
            var thisBatchOfItems: [Schools]?
            let start = self.offset

            NetworkAdapter.request(.schools(limit: self.itemsPerBatch, offset: start), type: Schools.self) { result in
                thisBatchOfItems = result
                // update UITableView with new batch of items on main thread after query finishes
                DispatchQueue.main.async {
                    if let newItems = thisBatchOfItems {
                        // append the new items to the data source for the table view
                        self.schoolsList.append(contentsOf: newItems)
                        
                        // reset the offset for the next data query
                        self.offset += self.itemsPerBatch
                        completion()
                    } else {
                        self.reachedEndOfItems = true
                    }
                }
            }

        }
    }
}
