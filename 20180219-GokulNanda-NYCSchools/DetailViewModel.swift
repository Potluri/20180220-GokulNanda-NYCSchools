//
//  MasterViewModel.swift
//  20180219-GokulNanda-NYCSchools
//
//  Created by GokulNanda on 20/02/18.
//  Copyright © 2018 GokulNanda. All rights reserved.
//

import Foundation
import Gloss

class DetailViewModel {
    var satResults = [Scores]()
    
    func getSATResults(dbn: String, completion: @escaping () -> Void) {
        NetworkAdapter.request(.satResults(dbn: dbn), type: Scores.self) { result in
            self.satResults = result
            completion()
        }
    }
    
    func numberOfSectionsToDisplay() -> Int {
        return 1
    }
    
    func numberOfRowsToDisplay(in section: Int) -> Int {
        return satResults.count
    }
}
