//
//  BaseClass.swift
//
//  Created by GokulNanda on 20/02/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import Gloss

public struct Scores: Glossy {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let schoolName = "school_name"
        static let satMathAvgScore = "sat_math_avg_score"
        static let satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        static let satWritingAvgScore = "sat_writing_avg_score"
        static let numOfSatTestTakers = "num_of_sat_test_takers"
        static let dbn = "dbn"
    }
    
    // MARK: Properties
    public var schoolName: String?
    public var satMathAvgScore: String?
    public var satCriticalReadingAvgScore: String?
    public var satWritingAvgScore: String?
    public var numOfSatTestTakers: String?
    public var dbn: String?
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public init(json: JSON) {
        schoolName = SerializationKeys.schoolName <~~ json
        satMathAvgScore = SerializationKeys.satMathAvgScore <~~ json
        satCriticalReadingAvgScore = SerializationKeys.satCriticalReadingAvgScore <~~ json
        satWritingAvgScore = SerializationKeys.satWritingAvgScore <~~ json
        numOfSatTestTakers = SerializationKeys.numOfSatTestTakers <~~ json
        dbn = SerializationKeys.dbn <~~ json
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = schoolName { dictionary[SerializationKeys.schoolName] = value }
        if let value = satMathAvgScore { dictionary[SerializationKeys.satMathAvgScore] = value }
        if let value = satCriticalReadingAvgScore { dictionary[SerializationKeys.satCriticalReadingAvgScore] = value }
        if let value = satWritingAvgScore { dictionary[SerializationKeys.satWritingAvgScore] = value }
        if let value = numOfSatTestTakers { dictionary[SerializationKeys.numOfSatTestTakers] = value }
        if let value = dbn { dictionary[SerializationKeys.dbn] = value }
        return dictionary
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            ])
    }
}
