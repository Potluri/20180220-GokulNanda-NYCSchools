//
//  BaseClass.swift
//
//  Created by GokulNanda on 20/02/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import Gloss

public struct Schools: Glossy {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let location = "location"
        static let schoolName = "school_name"
        static let dbn = "dbn"
    }
    
    // MARK: Properties
    public var location: String?
    public var schoolName: String?
    public var dbn: String?
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public init(json: JSON) {
        location = SerializationKeys.location <~~ json
        schoolName = SerializationKeys.schoolName <~~ json
        dbn = SerializationKeys.dbn <~~ json
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = location { dictionary[SerializationKeys.location] = value }
        if let value = schoolName { dictionary[SerializationKeys.schoolName] = value }
        if let value = dbn { dictionary[SerializationKeys.dbn] = value }
        return dictionary
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            ])
    }
}
